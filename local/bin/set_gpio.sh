#!/bin/sh

if [ -z "${1}" -o "${1}" != "rpi" -a "${1}" != "bpi" -a "${1}" != "opih3" ]; then
	echo "Usage : $(basename "${0}") [MODE - rpi,bpi,opih3]"
	exit 1
fi

GPIO_DIR=/app/gpio/gpio_${1}
if [ -e ${GPIO_DIR} ]; then
	mkdir -p /usr/local/bin /usr/local/lib /usr/local/include
	rm -rf /usr/local/bin/gpio /usr/local/bin/libwiringPi.so /usr/local/bin/libwiringPiDev.so
	find /usr/local/include -type l | while read -r line
	do
		l="$(readlink "${line}")"
		if [ -z "${l}" ]; then
			continue
		fi
		if [ "${l/gpio_/}" != "${l}" ]; then
			rm -rf "${line}"
		fi
	done
	ln -sf ${GPIO_DIR}/bin/gpio /usr/local/bin/gpio
	ln -sf ${GPIO_DIR}/lib/libwiringPi.so.* /usr/local/lib/libwiringPi.so
	ln -sf ${GPIO_DIR}/lib/libwiringPiDev.so.* /usr/local/lib/libwiringPiDev.so
	for inc in ${GPIO_DIR}/include/*
	do
		ln_t=$(basename "${inc}")
		ln -sf ${inc} /usr/local/include/${ln_t}
	done
fi

if [ -e /app/gpio/etc/cpuinfo_${1} ]; then
	mkdir -p /usr/local/etc
	ln -sf /app/gpio/etc/cpuinfo_${1} /usr/local/etc/cpuinfo
fi

